# LibreCash Website

## License

Copyright (C) 2017 Yasuhiro Asaka and Misaki Asaka

This is free software;

You can redistribute it and/or modify it under the terms of  
the GNU Affero General Public License (AGPL).

See doc/LICENSE.
